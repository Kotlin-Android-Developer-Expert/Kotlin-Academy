package com.hendisantika.kotlinacademy

/**
 * Created by hendisantika on 30/08/18  06.27.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
data class Item (val name: String?, val image: Int?)